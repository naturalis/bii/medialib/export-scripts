FROM python:3.6.1-alpine

RUN apk add --no-cache \
    bash=4.3.42-r5 \
    mariadb-client=10.1.32-r0 \
    zip=3.0-r4 \
    curl=7.60.0-r1

RUN mkdir /opt /opt/minio_client
RUN curl https://dl.min.io/client/mc/release/linux-amd64/mc --output /opt/minio_client/mc
RUN chmod +x /opt/minio_client/mc

COPY ./code /app

CMD ["/app/run.sh"]
