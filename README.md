# export scripts

Various dockerized export scripts for the medialib

Container contains three scripts:

+ **museumapp_brahms_numbers**: generates a list of all UnitID's from Brahms in the Medialibrary for the Museumapp-pipeline
+ **nba_medialib_cache_creator**: generates a list of all valid UnitID's in the Medialibrary for the NBA ETL
+ **nba_mimetype_cache_creator**: generates a list of images in the Medialibrary and their mimetype for the NBA ETL


## Container image
The container image is built automatically. Container registry:

https://gitlab.com/naturalis/bii/medialib/export-scripts/container_registry



## Running jobs
Copy template.env to .env, change env variables to production values, and run, repectively:

```
docker run --env-file .env -e RUN_SCRIPT=museumapp_brahms_numbers export-scripts:latest
docker run --env-file .env -e RUN_SCRIPT=nba_medialib_cache_creator export-scripts:latest
docker run --env-file .env -e RUN_SCRIPT=generate_mimetype_cache export-scripts:latest
```