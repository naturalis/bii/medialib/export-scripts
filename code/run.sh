#!/bin/bash

/opt/minio_client/mc alias set "$S3_NBA_BRONDATA_ALIAS" "$S3_NBA_BRONDATA_URL" "$S3_NBA_BRONDATA_ACCESS" "$S3_NBA_BRONDATA_SECRET" --api S3v4

if [[ "$RUN_SCRIPT" == "generate_mimetype_cache" ]]; then
    /app/nba_mimetype_cache_creator/generate_mimetype_cache.sh
fi

if [[ "$RUN_SCRIPT" == "nba_medialib_cache_creator" ]]; then
    /app/nba_medialib_cache_creator/generate_medialib_cache.sh
fi

if [[ "$RUN_SCRIPT" == "museumapp_brahms_numbers" ]]; then
    /app/museumapp_brahms_numbers/get_brahms_numbers.sh
    /app/museumapp_brahms_numbers/run_sql_statements.sh
    # curl -XPOST --data @/app/museumapp_brahms_numbers/results.txt https://$MUSEUMAPP_USER:$MUSEUMAPP_PASS@$MUSEUMAPP_DOMAIN/post_brahms.php
    # curl -XPOST --data @/app/museumapp_brahms_numbers/results.txt http://$MUSEUMAPP_USER:$MUSEUMAPP_PASS@$MUSEUMAPP_DEV_DOMAIN/post_brahms.php
fi
