#!/bin/bash

SQLFILE=/app/nba_mimetype_cache_creator/regno_filename_export.sql
OUTFILE=/app/nba_mimetype_cache_creator/mimetype_cache.txt
ZIPFILE=/app/nba_mimetype_cache_creator/mimetypes.zip
TMPFILE=/app/nba_mimetype_cache_creator/tmp
PY_FILE=/app/nba_mimetype_cache_creator/get_mimetype.py

if [[ -f "$ZIPFILE" ]]; then
    rm $ZIPFILE
fi

if [[ -f "$TMPFILE" ]]; then
    rm $TMPFILE
fi

mysql -N -B -s -u "$MYSQL_USER" -p"$MYSQL_PASSWORD" "$MYSQL_DATABASE" < $SQLFILE | while read -r line; do
  echo "$line" >> $TMPFILE
done

python $PY_FILE $TMPFILE "|" > $OUTFILE

zip -mj $ZIPFILE $OUTFILE

/opt/minio_client/mc cp $ZIPFILE "$S3_NBA_BRONDATA_ALIAS""$S3_NBA_BRONDATA_FOLDER"
