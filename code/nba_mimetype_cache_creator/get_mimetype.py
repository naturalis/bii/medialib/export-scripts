import sys, os

# from mimetypes import MimeTypes

# def __get_mime(path):
#     mime = MimeTypes()
#     return mime.guess_type(path)[0]


mime_map = [
    { "mime" : "image/jpeg", "extensions" : [ ".jpg", ".jpeg", ".jpe", ".jfif", ".jif" ] },
    { "mime" : "audio/mp3", "extensions" : [ ".mp3" ] },
    { "mime" : "image/png", "extensions" : [ ".png" ] },
    { "mime" : "video/mp4", "extensions" : [ ".mp4", ".m4a", ".m4p", ".m4b", ".m4r", ".m4v" ] },
    { "mime" : "application/pdf", "extensions" : [ ".pdf" ] }
]

unknown_mime = "application/octet-stream"

def __get_mime(filename):
    name, extension = os.path.splitext(filename)
    extension = extension.lower()
    match = [i for i in mime_map if extension in i["extensions"]]
    if match:
        return match[0]["mime"]
    else:
        return unknown_mime


if __name__ == '__main__':

    if len(sys.argv) >= 3:
        column_separator = sys.argv[2]
        with open(sys.argv[1]) as f:
            line = f.readline()
            while line:
                cells = line.strip().split(column_separator)
                # print(cells[0],cells[1],__get_mime(cells[1]))
                print(cells[0].strip())
                print(__get_mime(cells[1].strip()))
                line = f.readline()

            # for line in f.readlines():
            #     cells = line.strip().split(column_separator)
            #     # print(cells[0],cells[1],__get_mime(cells[1]))
            #     print(cells[0].strip())
            #     print(__get_mime(cells[1].strip()))

