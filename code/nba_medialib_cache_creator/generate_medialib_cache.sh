#!/bin/bash

SQLFILE=/app/nba_medialib_cache_creator/regno_export.sql
OUTFILE=/app/nba_medialib_cache_creator/medialib_ids.txt
ZIPFILE=/app/nba_medialib_cache_creator/medialib_ids_cache.zip

rm $ZIPFILE

mysql -N -s -u "$MYSQL_USER" -p"$MYSQL_PASSWORD" "$MYSQL_DATABASE" < $SQLFILE  >> $OUTFILE

zip -mj $ZIPFILE $OUTFILE

/opt/minio_client/mc cp $ZIPFILE "$S3_NBA_BRONDATA_ALIAS""$S3_NBA_BRONDATA_FOLDER"
