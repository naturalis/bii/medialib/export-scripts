#!/bin/bash

SQLFILE=/opt/museumapp_brahms_numbers/export.sql
OUTFILE=/opt/museumapp_brahms_numbers/results.txt

rm $OUTFILE

echo "data=" > $OUTFILE
mysql -N -s -u "$MYSQL_USER" -p"$MYSQL_PASSWORD" "$MYSQL_DATABASE" < $SQLFILE  >> $OUTFILE
