#!/bin/bash

SQL_FILE=export.sql

rm $SQL_FILE

curl -s -XGET https://"$MUSEUMAPP_USER":"$MUSEUMAPP_PASS"@pipeline-museumapp.naturalis.nl/brahms.php | while read -r line; do
  #echo "select concat('$line',',',regno,'||') as image_id from media where regno = '$line' or regno like '$line\_%';" >> $SQL_FILE

  # no_spaces=$(echo "$line" | sed 's/ \+//')
  no_spaces="${line// /}"

  echo "select concat('$line',',',regno,'||') as image_id from media where regno = '$no_spaces' or regno like '$no_spaces\_%';"  >> $SQL_FILE
done
